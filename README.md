# Trustworthy GPS and IMU

## Abstract:
GPS and IMU sensors are indispensible for location data on smartphones. Applications and services that depend on GPS and IMU data requires a level of trust in the sensors to operate reliably. GPS spoofing in particular is a common and widely available practice. Our project seeks to add authentication to GPS data such that a smartphone's location cannot be spoofed.

## Wiki:
[See more here](https://gitlab.com/vwong/trustworthy-gps-and-imu/wikis/home)